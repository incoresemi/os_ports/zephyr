/*
Copyright (c) 2012-2020, InCore Semiconductors Pvt. Ltd.
*/

/*
 * @brief UART driver for the InCore ChromiteM SoC
 */
#define DT_DRV_COMPAT incore_uart0

#include <kernel.h>
#include <arch/cpu.h>
#include <drivers/uart.h>

/*! UART Configuration registers description */
#define CONTROL_TX_THLD_GT         1 << 12
#define CONTROL_RX_THLD_GT         1 << 13
#define CONTROL_TX_THLD(x)         (x & 0x0F) << 16
#define CONTROL_RX_THLD(x)         (x & 0x0F) << 20
#define STATUS_TX_DONE             1 << 0
#define STATUS_TX_NOTFULL          1 << 1
#define STATUS_RX_NOTFULL          1 << 2
#define STATUS_RX_NOTEMPTY         1 << 3
#define STATUS_TX_THLD             1 << 4
#define STATUS_RX_THLD             1 << 5
#define IE_RX_NOT_EMPTY            1 << 3
#define IE_TX_THLD                 1 << 4
#define IE_RX_THLD                 1 << 5
#define IE_ANY_ERR                 0xF << 8

struct uart_incore_regs_t
{
	uint32_t baud;      /* Baud rate configuration Register */
  uint32_t tx_reg;    /* Transmit register -- the value that needs to be                                 
                      tranmitted needs to be written here */
  uint32_t rx_reg;    /* Receive register -- the value that received from uart can be read from here */ 
  uint32_t status;    /* Status register -- Reads various transmit and receive status */
  uint32_t control;   /* Control Register -- Configures the no. of bits used, stop bits, parity enabled or not */
  uint32_t status_clear; /* Status Clear Register -- Clears the interrupts that have been set */
  uint32_t interrupt_en; /* Interrupt Enable Register -- Configures the events for which an interrupt should be raised */
};

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
typedef void (*irq_cfg_func_t)(void);
#endif

struct uart_incore_device_config
{
	uintptr_t	port;
	uint32_t		sys_clk_freq;
	uint32_t		baud_rate;
  uint32_t   txthld;
  uint32_t   rxthld;
  #ifdef CONFIG_UART_INTERRUPT_DRIVEN
  irq_cfg_func_t cfg_func;
  #endif
};

struct uart_incore_data {
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	uart_irq_callback_user_data_t callback;
	void *cb_data;
#endif
};

#define DEV_CFG(dev)                                            \
        ((const struct uart_incore_device_config * const)       \
         (dev)->config_info)
#define DEV_UART(dev)                                           \
        ((struct uart_incore_regs_t *)(DEV_CFG(dev))->port)
#define DEV_DATA(dev)                                           \
        ((struct uart_incore_data * const)(dev)->driver_data)

/**
 * @brief Function to write a single character to the standard output device.
 * @details This function will be called to print a single character to the stdout by passing 
 * character as an integer.
 * @warning 
 * @param[in] character.
 * @param[Out] No output parameters.
 */
static void uart_incore_poll_out(struct device *dev, unsigned char c)
{
        volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

        /* Wait while TX FIFO is full */
        while (!(uart->status & STATUS_TX_NOTFULL));
        uart->tx_reg = (unsigned char)c;
}

/**
 * @brief Function to read a single character from the standard input device.
 * @details This function will be called to write a single character from the stdin.
 * @warning 
 * @param[in] Character pointer to store the input character.
 * @param[Out] No output parameters.
 */
static int uart_incore_poll_in(struct device *dev, unsigned char *c)
{
  volatile struct uart_incore_regs_t *uart = DEV_UART(dev);
  // return -1 is RX buffer is empty
	if (!(uart->status & STATUS_RX_NOTEMPTY)) {
    return -1;
  }
	  *c = (unsigned char) uart->rx_reg;
    return 0;
}
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
                                 
                                                              
/**
 * @brief Fill FIFO with data
 *
 * @param dev UART device struct
 * @param tx_data Data to transmit
 * @param size Number of bytes to send
 *
 * @return Number of bytes sent
 */
static int uart_incore_fifo_fill(struct device *dev, const uint8_t *tx_data, int size)
{
        volatile struct uart_incore_regs_t *uart = DEV_UART(dev);
        int i;
        for (i = 0; i < size && (uart->status & STATUS_TX_NOTFULL); i++)
	      {
                uart->tx_reg = (uint8_t) tx_data[i];
      	}
        return i;
}

/**
 * @brief Read data from FIFO
 *
 * @param dev UART device struct
 * @param rxData Data container
 * @param size Container size
 *
 * @return Number of bytes read
 */
static int uart_incore_fifo_read(struct device *dev, uint8_t *rx_data, const int size)
{
  volatile struct uart_incore_regs_t *uart = DEV_UART(dev);
  int i;
  uint32_t val;
  for (i = 0; i < size; i++) {
		if((uart->status & STATUS_RX_NOTEMPTY) == 0)
		{
			break;
		}
    val = uart->rx_reg;
    rx_data[i] = (uint8_t)val;
  }
  return i;
}

static void uart_incore_irq_handler(void *arg)
{
	struct device *dev = (struct device *)arg;
	struct uart_incore_data *data = DEV_DATA(dev);

	if (data->callback)
		data->callback(data->cb_data);
}

/**
 * @brief Enable TX interrupt in ie register
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_tx_enable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en |= IE_TX_THLD;
}

/**
 * @brief Disable TX interrupt in ie register
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_tx_disable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en &= ~IE_TX_THLD;
}

/*
 * @brief Check if Tx threshold status bit has been set
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ is ready, 0 otherwise
 */
static int uart_incore_irq_tx_ready(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	return !!(uart->status & STATUS_TX_THLD);
}

/*
 * @brief Enable RX interrupt in ie register
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_rx_enable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en |= IE_RX_THLD;
}

/*
 * @brief Disable RX interrupt in ie register
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_rx_disable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en &= ~IE_RX_THLD;
}

/*
 * @brief Check if nothing remains to be transmitted
 *
 * @param dev UART device struct
 *
 * @return 1 if nothing remains to be transmitted, 0 otherwise
 */
static int uart_incore_irq_tx_complete(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	return !!(uart->status & STATUS_TX_DONE);
}

/*
 * @brief Check if Rx threshold status has been set
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ is ready, 0 otherwise
 */
static int uart_incore_irq_rx_ready(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	return !!(uart->status & STATUS_RX_THLD);
}

/*
 * @brief Enable interrupts on any errors
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_err_enable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en |= IE_ANY_ERR;
}

/*
 * @brief Disable interrupts on any errors
 *
 * @param dev UART device struct
 *
 * @return N/A
 */
static void uart_incore_irq_err_disable(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	uart->interrupt_en &= ~IE_ANY_ERR;
}

/*
 * @brief Check if any of the error status flags has been raised
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ is ready, 0 otherwise
 */
static int uart_incore_irq_is_pending(struct device *dev)
{
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

	return !!(uart->status & uart->interrupt_en);
}

static int uart_incore_irq_update(struct device *dev)
{
	return 1;
}

/**
 * @brief Set the callback function pointer for IRQ.
 *
 * @param dev UART device struct
 * @param cb Callback function pointer.
 *
 * @return N/A
 */
static void uart_incore_irq_callback_set(struct device *dev,
					uart_irq_callback_user_data_t cb,
					void *cb_data)
{
	struct uart_incore_data *data = DEV_DATA(dev);

	data->callback = cb;
	data->cb_data = cb_data;
}

#endif /* CONFIG_UART_INTERRUPT_DRIVEN */

static int uart_incore_init(struct device *dev)
{
	const struct uart_incore_device_config * const cfg = DEV_CFG(dev);
	volatile struct uart_incore_regs_t *uart = DEV_UART(dev);

  //TODO add baud init
  //volatile u64_t ctrl_val= (u64_t) uart->control; 
  //ctrl_val &= ~(CONTROL_TX_THLD(0x0F));         //Clear the existing TX threshold
  //ctrl_val &= ~(CONTROL_RX_THLD(0x0F));         //Clear the existing RX threshold
  //ctrl_val  = ctrl_val | CONTROL_TX_THLD(cfg->txthld); //Set TX threshold value
  //ctrl_val &= ~CONTROL_TX_THLD_GT;           //Set TX dirn to be <=
  //ctrl_val  = ctrl_val | CONTROL_RX_THLD(cfg->rxthld); //Set RX threshold value
  //ctrl_val |= CONTROL_RX_THLD_GT;            //Set RX dirn to be >
  //uart->control = ctrl_val;
  uart->interrupt_en= 0U;                    //Clear all interrupt enables

	/* Setup IRQ handler */
	cfg->cfg_func();

	return 0;
}

static const struct uart_driver_api uart_incore_driver_api = 
{
  .poll_in   = uart_incore_poll_in,
  .poll_out  = uart_incore_poll_out,
  .err_check = NULL,
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
  .fifo_fill           = uart_incore_fifo_fill,
  .fifo_read           = uart_incore_fifo_read,
  .irq_tx_enable       = uart_incore_irq_tx_enable,
  .irq_tx_disable      = uart_incore_irq_tx_disable,
  .irq_tx_ready        = uart_incore_irq_tx_ready,
  .irq_tx_complete     = uart_incore_irq_tx_complete,
  .irq_rx_enable       = uart_incore_irq_rx_enable,
  .irq_rx_disable      = uart_incore_irq_rx_disable,
	.irq_rx_ready        = uart_incore_irq_rx_ready,
	.irq_err_enable      = uart_incore_irq_err_enable,
	.irq_err_disable     = uart_incore_irq_err_disable,
	.irq_is_pending      = uart_incore_irq_is_pending,
	.irq_update          = uart_incore_irq_update,
	.irq_callback_set    = uart_incore_irq_callback_set,
#endif
};

#ifdef CONFIG_UART_INCORE_PORT_0

static struct uart_incore_data uart_incore_data_0;

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
static void uart_incore_irq_cfg_func_0(void);
#endif

static const struct uart_incore_device_config uart_incore_dev_cfg_0 = 
{
  .port         = DT_INST_REG_ADDR(0),
  .sys_clk_freq = DT_INST_PROP(0, clock_frequency),
  .baud_rate    = DT_INST_PROP(0, current_speed),
  .txthld       = CONFIG_UART_INCORE_PORT_0_TX_THLD,
  .rxthld       = CONFIG_UART_INCORE_PORT_0_RX_THLD,
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	.cfg_func     = uart_incore_irq_cfg_func_0,
#endif
};

DEVICE_AND_API_INIT(uart_incore_0, DT_INST_LABEL(0),
                    uart_incore_init,
                    &uart_incore_data_0, &uart_incore_dev_cfg_0,
                    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
                    (void *)&uart_incore_driver_api);

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
static void uart_incore_irq_cfg_func_0(void)
{
	IRQ_CONNECT(DT_INST_IRQN(0),
		    CONFIG_UART_INCORE_PORT_0_IRQ_PRIORITY,
		    uart_incore_irq_handler, DEVICE_GET(uart_incore_0),
		    0);

	irq_enable(DT_INST_IRQN(0));
}
#endif


#endif /* CONFIG_UART_INCORE_PORT_0 */

