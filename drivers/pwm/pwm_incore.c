/*
 * Copyright (c) 2012-2020, InCore Semiconductors Pvt. Ltd.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT incore_pwm0

#include <logging/log.h>

LOG_MODULE_REGISTER(pwm_incore, CONFIG_PWM_LOG_LEVEL);

#include <sys/sys_io.h>
#include <zephyr/types.h>
#include <syscall_handler.h>
#include <device.h>
#include <drivers/pwm.h>
#include <drivers/pwm/pwm_incore.h>

/* Structure Declarations */
// struct pwm_incore_regs_t{};

#ifdef CONFIG_PWM_INTERRUPT_DRIVEN
typedef void (*irq_cfg_func_t)(void);
#endif

struct pwm_incore_cfg {
    uint32_t base;
    uint32_t f_sys;
    uint32_t pwmwidth;
    uint32_t prescale;
    uint32_t comp_output;
    uint32_t clock_source;
    uint32_t channel_count;
    uint32_t deadband_value;
#ifdef CONFIG_PWM_INTERRUPT_DRIVEN
    irq_cfg_func_t cfg_func;
#endif
};

struct pwm_incore_data {
#ifdef CONFIG_PWM_INTERRUPT_DRIVEN
    pwm_irq_callback_user_data_t callback;
    void *cb_data;
#endif
};

/* Helper Macros for PWM */
#define DEV_CFG(dev)                        \
    ((const struct pwm_incore_cfg * const)(dev)->config_info)
/* #define DEV_PWM(dev)                     \
    ((volatile struct pwm_incore_regs_t *)(DEV_CFG(dev))->base)
*/
#define DEV_PWM_DATA(dev)               \
    ((struct pwm_incore_data *)(dev)->driver_data)

/* API Functions */

static int pwm_incore_init(struct device *dev)
{
    const struct pwm_incore_cfg *config = DEV_CFG(dev);
//  volatile struct pwm_incore_regs_t *pwm = DEV_PWM(dev);
    // Clear the registers fields.
    if (config == NULL) {
        LOG_ERR("The device configuration is NULL\n");
        return -EFAULT;
    }
    if (config->prescale == 0 || config -> prescale > CLOCK_PRESCALAR_MAX) {
        LOG_ERR("INVALID Prescalar value\n");
        return -EINVAL;
    }
    volatile uint16_t clock_reg = ((config->prescale) << 1 | config ->clock_source );
    sys_write32(clock_reg, PWM_REG(config, REG_CLOCK));
    //LOG_INF("Init PWM module Frequency: %d, Prescale: %d, clock_source: %d, deadband: %d",
    //    config->f_sys, config->prescale, config ->clock_source, config->deadband_value);
    /* Clear all channels */
    for (int i = 0; i < config->channel_count; i++) {
        /* Clear the channel related registers */
        sys_write32(0, PWM_REG(config, REG_PERIOD(i)));
        sys_write32(0, PWM_REG(config, REG_DUTY(i)));
        sys_write32(config->deadband_value, PWM_REG(config, REG_DEAD_BAND(i)));
        sys_write32(0, PWM_REG(config, REG_CONTROL(i)));
        //LOG_INF("Initializing channel: [%d]",i);
    }
    return 0;
}

static int pwm_incore_pin_set(struct device *dev,
                  uint32_t pwm,
                  uint32_t period_cycles,
                  uint32_t pulse_cycles,
                  pwm_flags_t flags)

{   const struct pwm_incore_cfg *config = NULL;
    uint32_t count_max = 0U;
    uint32_t pwm_flags = 0U;
/*    LOG_DBG("START: channel: [%d], Pulse Width: %d, Dutycycle: %d",
        pwm, period_cycles, pulse_cycles);
*/    if (dev == NULL) {
        LOG_ERR("The device instance pointer was NULL\n");
        return -EFAULT;
    }

    config = DEV_CFG(dev);
    if (config == NULL) {
        LOG_ERR("The device configuration is NULL\n");
        return -EFAULT;
    }

    if (pwm >= config->channel_count) {
        LOG_ERR("The requested PWM channel %d is invalid\n", pwm);
        return -EINVAL;
    }

    if ((flags & (1 << PWM_OUTPUT_COMPLEMNT)) && (!(config -> comp_output))) {
        LOG_ERR("complementary output is disabled");
        return -ENOTSUP;
    }

    /* We can't support periods greater than we can store in pwmcount */
    count_max = (1 << (config->pwmwidth + 1)) - 1;

    if (period_cycles > count_max) {
        LOG_ERR("Requested period is %d but maximum is %d\n",
            period_cycles, count_max);
        return -EIO;
    }

    if ((period_cycles == 0U) || pulse_cycles > period_cycles) {
        LOG_ERR("Requested pulse %d is longer than period %d\n",
            pulse_cycles, period_cycles);
        return -EIO;
    }

    /* Set the period */
    sys_write32(period_cycles, PWM_REG(config, REG_PERIOD(pwm)));

    /* Set the duty cycle */
    sys_write32(pulse_cycles,  PWM_REG(config, REG_DUTY(pwm)));

    if((period_cycles == 0U) || (pulse_cycles == 0U))
        pwm_flags = (((flags & 0xFC) << 1) | 0);
    else
        pwm_flags = ((1 << PWM_LOAD_VALUES) | 1 << RESET_COUNTER | \
                     ((flags& 0xFC) << 1)   | (1 << PWM_ENABLE));

    /* Set the configuration register */
    sys_write32(pwm_flags, PWM_REG(config, REG_CONTROL(pwm)));

    pwm_flags = ((flags << 1) | (1 << PWM_ENABLE));
    sys_write32(pwm_flags, PWM_REG(config, REG_CONTROL(pwm)));

    //LOG_INF("END: channel : [%d], Pulse Width: %d, Dutycycle: %d Flags: %x",
    //    pwm, period_cycles, pulse_cycles, pwm_flags);

    return 0;
}

static int pwm_incore_get_cycles_per_sec(struct device *dev,
                     uint32_t pwm,
                     uint64_t *cycles)
{
    const struct pwm_incore_cfg *config;

    if (dev == NULL) {
        LOG_ERR("The device instance pointer was NULL\n");
        return -EFAULT;
    }

    config = DEV_CFG(dev);
    if (config == NULL) {
        LOG_ERR("The device configuration is NULL\n");
        return -EFAULT;
    }

    /* Fail if we don't have that channel */
    if (pwm >= config->channel_count) {
        return -EINVAL;
    }

    *cycles = ((config->f_sys) / (config -> prescale));

    return 0;
}

/* Incore's PWM specific driver extenstion */

/* syscall API to set the deadband delay on -fly */
int z_impl_incore_pwm_set_deadband_delay(struct device *dev,
		uint32_t pwm, uint16_t deadband)
{
    const struct pwm_incore_cfg *config = NULL;

    if (dev == NULL) {
        LOG_ERR("The device instance pointer was NULL\n");
        return -EFAULT;
    }

    config = DEV_CFG(dev);
    if (config == NULL) {
        LOG_ERR("The device configuration is NULL\n");
        return -EFAULT;
    }
    if (pwm >= config->channel_count) {
        LOG_ERR("The requested PWM channel %d is invalid\n", pwm);
        return -EINVAL;
    }
#if 0
	if ((deadband > (period_cycles - pulse_cycles)) && (deadband > pulse_cycles)){
    	LOG_ERR("The requested deadband %d is greater the on/off period",deadband);
    	return -EINVAL;
    }
#endif
    /* Set the deadband value */
    sys_write32(deadband,  PWM_REG(config, REG_DEAD_BAND(pwm)));
    LOG_DBG("channel: [%d], Deadband: %d", pwm, deadband);
    return 0;
}

/* syscall API to set the clock prescaler and souce on -fly */
int z_impl_incore_pwm_set_clock_register(struct device *dev,
		uint16_t prescalar, bool clk_source)
{
	const struct pwm_incore_cfg *config = NULL;

    if (dev == NULL) {
        LOG_ERR("The device instance pointer was NULL\n");
        return -EFAULT;
    }

    config = DEV_CFG(dev);
    if (config == NULL) {
        LOG_ERR("The device configuration is NULL\n");
        return -EFAULT;
    }
    if (prescalar > CLOCK_PRESCALAR_MAX) {
        LOG_ERR("The requested Prescalar is invalid - Exceeds 0x%x \n", CLOCK_PRESCALAR_MAX);
        return -EINVAL;
    }
    /* Set the Clock value */
    sys_write32(((prescalar << 1 )|clk_source),  PWM_REG(config, REG_CLOCK));
    LOG_DBG("PWM Prescalar: [%d], Clock Source: %d", prescalar, clk_source);
    return 0;
}
/* Device Instantiation */

static const struct pwm_driver_api pwm_incore_api = {
    .pin_set = pwm_incore_pin_set,
    .get_cycles_per_sec = pwm_incore_get_cycles_per_sec,
};

#ifdef CONFIG_USERSPACE

#include <syscall_handler.h>
int z_vrfy_incore_pwm_set_deadband_delay(struct device *dev,
				 uint32_t pwm, uint16_t deadband)
{
    Z_OOPS(Z_SYSCALL_SPECIFIC_DRIVER(dev,K_OBJ_DRIVER_PWM, pwm_incore_init));
    return z_impl_incore_pwm_set_deadband_delay(dev, pwm, deadband);
}
#include <syscalls/incore_pwm_set_deadband_delay_mrsh.c>

int z_vrfy_incore_pwm_set_clock_register(struct device *dev,
				 uint16_t prescalar, bool clk_source)
{
    Z_OOPS(Z_SYSCALL_SPECIFIC_DRIVER(dev,K_OBJ_DRIVER_PWM, pwm_incore_init));
    return z_impl_incore_pwm_set_clock_register(dev, prescalar, clk_source);
}
#include <syscalls/incore_pwm_set_clock_register_mrsh.c>
#endif /* CONFIG_USERSPACE */

#define PWM_INCORE_INIT(n)                                          \
    static struct pwm_incore_data pwm_incore_data_##n;              \
    static const struct pwm_incore_cfg pwm_incore_cfg_##n = {       \
            .base           = DT_INST_REG_ADDR(n),                  \
            .f_sys          = DT_INST_PROP(n, clock_frequency),     \
            .pwmwidth       = DT_INST_PROP(n, incore_pwm_width),    \
            .comp_output    = DT_INST_PROP(n,incore_outbar_en),     \
            .prescale       = CLOCK_PRESCALAR,         			    \
            .clock_source   = CLOCK_SELECTOR,                       \
            .deadband_value = PWM_DEADBAND_VALUE,                   \
            .channel_count  = DT_INST_PROP(n, incore_channels)      \
        };                                                          \
    DEVICE_AND_API_INIT(pwm_incore_##n, DT_INST_LABEL(n),           \
                pwm_incore_init,                                    \
                &pwm_incore_data_##n,                               \
                &pwm_incore_cfg_##n,                                \
                POST_KERNEL,                                        \
                CONFIG_KERNEL_INIT_PRIORITY_DEVICE,        	        \
                &pwm_incore_api);

DT_INST_FOREACH_STATUS_OKAY(PWM_INCORE_INIT)

/* #ifdef CONFIG_PWM_INTERRUPT_DRIVEN
            .cfg_func     = pwm_incore_irq_cfg_func_##n,            \
#endif

#ifdef CONFIG_PWM_INTERRUPT_DRIVEN
    static void pwm_incore_irq_cfg_func_##n(void);                  \
#endif

#ifdef CONFIG_PWM_INTERRUPT_DRIVEN
    static void pwm_incore_irq_cfg_func_##n(void){                  \
        IRQ_CONNECT(DT_INST_IRQN(n),                                \
                CONFIG_PWM_INCORE_PORT_0_IRQ_PRIORITY,              \
                pwm_incore_irq_handler, DEVICE_GET(pwm_incore_##n), \
                0);                                                 \
        irq_enable(DT_INST_IRQN(n));                                \
    }                                                               \
#endif
*/
