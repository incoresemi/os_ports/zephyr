/*
 * Copyright (c) 2012-2020, InCore Semiconductors Pvt. Ltd.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT incore_spi0

#include <logging/log.h>

LOG_MODULE_REGISTER(spi_incore, CONFIG_SPI_LOG_LEVEL);

#include "spi_incore.h"

#include <stdbool.h>

/* Helper Functions */
int spi_incore_config(struct device *dev,
                   const struct spi_config *config)
{
    uint32_t div;
    /* Looping not supported */
    if (config -> operation & SPI_MODE_LOOP) {
        LOG_ERR("Loopback mode is not supported");
        return -ENOTSUP;
    }
    /* DUAL/QUAD/OCTAL SPI not supported */
    if ((config -> operation & SPI_LINES_MASK) != SPI_LINES_SINGLE) {
        LOG_ERR("Multiple lines are not supported");
        return -ENOTSUP;
    }

    /* Invalid Slave selection */
    if(config -> slave >= (SPI_CFG(dev)-> max_slaves)){
        LOG_ERR("Unsupported slave ID %u \t Max slave ID supported is %u",
                config -> slave, (SPI_CFG(dev)-> max_slaves));
        return -EINVAL;
    }

    /* GPIO based CS control is not supported */
    if(config -> cs != NULL){
        return -ENOTSUP;
    }

    /* Set the SPI frequency */
    if((config -> frequency) > (SPI_CFG(dev)->f_sys /2)){
        /* Saturate the Operating Frequency to SPI's Max. Frequency */
        div = IC_MIN_PRESCALE + 2;
    }else{
        div = (SPI_CFG(dev)->f_sys / (config -> frequency));
        div = (div > IC_MAX_PRESCALE) ? IC_MAX_PRESCALE : div ;
    }
    if (SPI_OP_MODE_GET(config -> operation) == SPI_OP_MODE_MASTER){
        sys_write32(div, SPI_REG(dev, REG_PRESCLR));
        LOG_INF("Requested Frequency is %uHz and SPI opted for %uHz",  \
          config -> frequency, (SPI_CFG(dev)->f_sys / ((div%2)? (div+1) : div)));
    }

    uint32_t cfg1_val = IC_SPI_CPHA(config -> operation & SPI_MODE_CPHA) | \
                        IC_SPI_CPOL(config -> operation & SPI_MODE_CPOL) | \
                        IC_SPI_MSTR(!SPI_OP_MODE_GET(config -> operation))| \
                        IC_SPI_LSBFIRST(config -> operation & SPI_TRANSFER_LSB) | \
                        IC_SPI_CLR_ST ;

    uint32_t cfg2_val = IC_SPI_PERIPH_SEL(config -> slave);

    sys_write32(cfg1_val, SPI_REG(dev, REG_CFG1));
    sys_write32(cfg2_val, SPI_REG(dev, REG_CFG2));
    return 0;
}

volatile uint32_t cfg1_val;
/* unified function to set CR1 for transfers */
static void spi_incore_set_regs(struct device *dev, uint8_t duplex_mode,
                     size_t tx_len_bits, size_t rx_len_bits){
    /* read value from register */
    cfg1_val = sys_read32(SPI_REG(dev, REG_CFG1));
    /* Clear Locations of interest in register */
    cfg1_val &= (~IC_SPI_TOTAL_BITS_RX_MASK) & (~IC_SPI_TOTAL_BITS_TX_MASK)
             &  (~IC_SPI_DPLX_MASK);
    /* Set Locations of interest in register */
    cfg1_val |= IC_SPI_TOTAL_BITS_RX((rx_len_bits > 255) ? 255 : rx_len_bits)
             |  IC_SPI_TOTAL_BITS_TX((tx_len_bits > 255) ? 255 : tx_len_bits)
             |  IC_SPI_DPLX(duplex_mode);
    /* write value to register */
    sys_write32(cfg1_val, SPI_REG(dev, REG_CFG1));
    sys_write32(1, SPI_REG(dev, REG_EN));
}
volatile uint32_t spi_status;
uint32_t spi_incore_recv_helper(struct device *dev){
    spi_status = sys_read32(SPI_REG(dev, REG_SR));
    while ((spi_status & IC_SPI_RXNE) == 0) {
        spi_status = sys_read32(SPI_REG(dev, REG_SR));
    }
    return sys_read32(SPI_REG(dev, REG_RXR));
}

void spi_incore_send_helper(struct device *dev, uint32_t tx_data_frame){
#if 0
    while(((sys_read32(SPI_REG(dev, REG_SR)) & IC_TX_FIFO) >> 16) > 16){
        /* NOP */
    }
#endif
    spi_status = sys_read32(SPI_REG(dev, REG_SR));
    while ((spi_status & IC_SPI_TXE) == 0) {
        spi_status = sys_read32(SPI_REG(dev, REG_SR));
    }
    sys_write32(tx_data_frame, SPI_REG(dev, REG_TXR));
}

void spi_incore_send(struct device *dev, bool lsbfirst)
{
    uint32_t tx_data_frame = 0U;
    uint8_t bytecount = (lsbfirst) ? 0 : 3 ;
    while(spi_context_tx_on(&SPI_DATA(dev)->ctx)){
        uint8_t tx_data  = 0U;
        if(spi_context_tx_buf_on(&SPI_DATA(dev)->ctx)){
            tx_data = UNALIGNED_GET((uint8_t *)(SPI_DATA(dev)->ctx.tx_buf));
        }
        tx_data_frame |= (tx_data << (bytecount <<3));
        bytecount = (lsbfirst) ? bytecount + 1 : bytecount - 1;
        spi_context_update_tx(&SPI_DATA(dev)->ctx, 1, 1);
        if((bytecount == 0 || bytecount == 3) || !spi_context_tx_on(&SPI_DATA(dev)->ctx)){
            bytecount = (lsbfirst) ? 0 : 3 ;
            spi_incore_send_helper(dev, tx_data_frame);
        }
    }
}

void spi_incore_recv(struct device *dev, bool lsbfirst)
{
    uint32_t rx_data_frame = 0U;
    uint8_t bytecount = (lsbfirst) ? 0 : 3 ;
    while(spi_context_rx_on(&SPI_DATA(dev)->ctx)){
        if(bytecount == 0 || bytecount == 3){
            bytecount = (lsbfirst) ? 0 : 3 ;
            rx_data_frame = (((sys_read32(SPI_REG(dev, REG_SR)) & IC_RX_FIFO) >> 8)) ?
                               spi_incore_recv_helper(dev) : 0U ;
        }
        uint8_t rx_data  = (rx_data_frame  >> (bytecount << 3));
        bytecount = (lsbfirst) ? bytecount + 1 : bytecount - 1;
        if(spi_context_rx_buf_on(&SPI_DATA(dev)->ctx)){
            UNALIGNED_PUT(rx_data, (uint8_t *)(SPI_DATA(dev)->ctx.rx_buf));
        }
        spi_context_update_rx(&SPI_DATA(dev)->ctx, 1, 1);
    }
}
void dummy_print(uint8_t tx_opcode, uint32_t data_frame){
    printk("Printing: %02x - %08x\n",tx_opcode, data_frame);
}

/* Fast path that writes and reads bufs of the same length */
static void spi_incore_fast_xfer(struct device *dev, const struct spi_buf *tx_buf,
                  const struct spi_buf *rx_buf, uint8_t duplex_mode, bool lsbfirst)
{
    uint32_t tx_data_frame = 0U;
    uint32_t rx_data_frame = 0U;
    bool send_now = false;
    uint8_t bytecount = (lsbfirst) ? 0 : 3 ;

    uint8_t *tx = tx_buf->buf;
    uint8_t *rx = rx_buf->buf;
    size_t len = tx_buf->len;

    if(UNALIGNED_GET((uint8_t *)tx_buf->buf) == 0x9F){
        if(k_uptime_get() < 2){
            /* wait a while, then let coop thread have a turn */
            k_busy_wait(600);
        }
    }

    spi_incore_set_regs(dev, duplex_mode, (tx_buf->len)<<3, (rx_buf->len)<<3);
    while(len){
        uint8_t tx_data = (!!(tx)) ? UNALIGNED_GET((uint8_t *)tx) : 0U;

        tx_data_frame |= (tx_data << (bytecount <<3));
        if(--len){
            tx++;
        }
        if((bytecount == 0 && !lsbfirst) || (bytecount == 3 && lsbfirst)){
            send_now = true;
        }else{
            bytecount = (lsbfirst) ? bytecount + 1 : bytecount - 1;
        }
        if((len == 0) || send_now){
            // dummy_print(UNALIGNED_GET((uint8_t *)tx_buf->buf), tx_data_frame);
            bytecount = (lsbfirst) ? 0 : 3 ;
            spi_incore_send_helper(dev, tx_data_frame);
            tx_data_frame = 0U;
            send_now = false;
        }
    }

    len = rx_buf->len;
    bytecount = (lsbfirst) ? 0 : 3 ;
    while(len){
        if((bytecount == 0 && lsbfirst) || (bytecount == 3 && !lsbfirst)){
            // bytecount = (lsbfirst) ? 0 : 3 ;
            rx_data_frame = spi_incore_recv_helper(dev) ;
            // dummy_print(UNALIGNED_GET((uint8_t *)tx_buf->buf), rx_data_frame);
        }
        uint8_t rx_data  = (rx_data_frame  >> (bytecount << 3));
        if((bytecount == 3 && lsbfirst) || (bytecount == 0 && !lsbfirst)){
            bytecount = (lsbfirst) ? 0 : 3 ;
        }else{
            bytecount = (lsbfirst) ? bytecount + 1 : bytecount - 1;
        }
        UNALIGNED_PUT(rx_data, (uint8_t *)(rx));
        if(--len)
            rx++;
    }
}

/* Common case where every tx is done after which rx
 * is received
 */
static void spi_incore_fast_transceive(struct device *dev,
                    const struct spi_buf_set *tx_bufs,
                    const struct spi_buf_set *rx_bufs,
                    bool lsbfirst)
{
    size_t tx_count = 0;
    size_t rx_count = 0;
    struct spi_buf *tx = NULL;
    struct spi_buf *rx = NULL;

    if (tx_bufs) {
        tx = (struct spi_buf*)tx_bufs->buffers;
        tx_count = tx_bufs->count;
    }

    if (rx_bufs) {
        rx = (struct spi_buf*)rx_bufs->buffers;
        rx_count = rx_bufs->count;
    }else{ // Simplex with write only
        rx = (struct spi_buf*)tx_bufs->buffers;
    }
    rx++;
    if(rx_count == 0){
        if(rx->len){
            memcpy((uint8_t*)(tx->buf) + (tx->len),
                   (uint8_t*)(rx->buf), (rx->len));
            tx->len += rx->len;
            rx->buf = NULL;
            rx->len = 0;
        }
    }
    // (rx_count == 0) ? IC_DUPLEX
    spi_incore_fast_xfer(dev, tx, rx, IC_SIMPLEX, lsbfirst);
}

void spi_incore_xfer(struct device *dev,
                     const struct spi_buf_set *tx_bufs,
                     const struct spi_buf_set *rx_bufs,
                     bool lsbfirst)
{
    size_t tx_count = 0;
    size_t rx_count = 0;
    const struct spi_buf *tx = NULL;
    const struct spi_buf *rx = NULL;

    if (tx_bufs) {
        tx = tx_bufs->buffers;
        tx_count = tx_bufs->count;
    }

    if (rx_bufs) {
        rx = rx_bufs->buffers;
        rx_count = rx_bufs->count;
    }

    if(tx_count == 1 || tx_count == 2){ //Common case for Memory Read
        spi_incore_fast_transceive(dev, tx_bufs, rx_bufs, lsbfirst);
    }
    else {
        spi_context_buffers_setup(&SPI_DATA(dev)->ctx, tx_bufs, rx_bufs, 1);
        size_t tx_len = spi_context_total_tx_len(&SPI_DATA(dev)->ctx) << 3;
        size_t rx_len = spi_context_total_rx_len(&SPI_DATA(dev)->ctx) << 3;

        if(rx_count == 0 && tx_count != 0){
            spi_incore_set_regs(dev, IC_SIMPLEX, tx_len, rx_len);
            spi_incore_send(dev, lsbfirst);
        }
        else if(tx_count == 0 && rx_count != 0){
            spi_incore_set_regs(dev, IC_SIMPLEX, tx_len, rx_len);
            spi_incore_recv(dev, lsbfirst);
        }
        else if(tx_count != 0 && rx_count != 0){
            spi_incore_set_regs(dev, IC_DUPLEX, tx_len, rx_len);
            spi_incore_send(dev, lsbfirst);
            spi_incore_recv(dev, lsbfirst);
        }
        else
            LOG_ERR("Nothing to Transfer as Both Buffers are empty");
    }
    spi_context_complete(&SPI_DATA(dev)->ctx, 0);
}

/* API Functions */
int spi_incore_init(struct device *dev)
{
    /* Make sure the context is unlocked */
    spi_context_unlock_unconditionally(&SPI_DATA(dev)->ctx);

    /* device will be configured and enabled when transceive
     * is called.
     */
    sys_write32(IC_SPI_CLR_ST, SPI_REG(dev, REG_CFG1));
    return 0;
}

int spi_incore_transceive(struct device *dev,
              const struct spi_config  *config,
              const struct spi_buf_set *tx_bufs,
              const struct spi_buf_set *rx_bufs)
{
    uint8_t ret = 0;
    if (!tx_bufs && !rx_bufs) {
        return 0;
    }

    /* Lock the SPI Context */
    // spi_context_lock(&SPI_DATA(dev)->ctx, false, NULL, config);
    spi_context_lock(&SPI_DATA(dev)->ctx, false, NULL);

    /* Configure the SPI bus */
    SPI_DATA(dev)->ctx.config = config;

    ret = spi_incore_config(dev, config);
    if (ret) {
        spi_context_release(&SPI_DATA(dev)->ctx, ret);
        return ret;
    }

    spi_incore_xfer(dev, tx_bufs, rx_bufs,
        IC_SPI_LSBFIRST(config -> operation & SPI_TRANSFER_LSB));

    ret = spi_context_wait_for_completion(&SPI_DATA(dev)->ctx);

    spi_context_release(&SPI_DATA(dev)->ctx, ret);

    return ret;
}

#ifdef CONFIG_SPI_ASYNC
static int spi_incore_transceive_async(struct device *dev,
              const struct spi_config *config,
              const struct spi_buf_set *tx_bufs,
              const struct spi_buf_set *rx_bufs,
              struct k_poll_signal *async)
{
    return -ENOTSUP;
}
#endif /* CONFIG_SPI_ASYNC */

int spi_incore_release(struct device *dev, const struct spi_config *config)
{
    spi_context_unlock_unconditionally(&SPI_DATA(dev)->ctx);
    return 0;
}

/* Device Instantiation */

static struct spi_driver_api spi_incore_api = {
    .transceive = spi_incore_transceive,
#ifdef CONFIG_SPI_ASYNC
    .transceive_async = spi_incore_transceive_async,
#endif /* CONFIG_SPI_ASYNC */
    .release = spi_incore_release,
};

#define SPI_INCORE_INIT(n) \
    static struct spi_incore_data spi_incore_data_##n = {       \
        SPI_CONTEXT_INIT_LOCK(spi_incore_data_##n, ctx),        \
        SPI_CONTEXT_INIT_SYNC(spi_incore_data_##n, ctx)         \
    };                                                          \
    static const struct spi_incore_cfg spi_incore_cfg_##n = {   \
        .base = DT_INST_REG_ADDR_BY_NAME(n, control),           \
        .f_sys = DT_INST_PROP(n, clock_frequency),              \
        .max_slaves = DT_INST_PROP(n, incore_max_slaves),       \
    };                                                          \
    DEVICE_AND_API_INIT(spi_incore_##n,                         \
            DT_INST_LABEL(n),                                   \
            spi_incore_init,                                    \
            &spi_incore_data_##n,                               \
            &spi_incore_cfg_##n,                                \
            POST_KERNEL,                                        \
            CONFIG_SPI_INIT_PRIORITY,                           \
            &spi_incore_api)

//DT_INST_FOREACH_STATUS_OKAY(SPI_INCORE_INIT)

// #ifndef CONFIG_INCORE_SPI_0_ROM
#if DT_INST_NODE_HAS_PROP(0, label)

    SPI_INCORE_INIT(0);

#endif // DT_INST_NODE_HAS_PROP(0, label)
// #endif // !CONFIG_INCORE_SPI_0_ROM

#if DT_INST_NODE_HAS_PROP(1, label)

   SPI_INCORE_INIT(1);

#endif // DT_INST_NODE_HAS_PROP(1, label)

#if DT_INST_NODE_HAS_PROP(2, label)

   SPI_INCORE_INIT(2);

#endif // DT_INST_NODE_HAS_PROP(2, label)
