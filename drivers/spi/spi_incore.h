/*
 * Copyright (c) 2012-2020, InCore Semiconductors Pvt. Ltd.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _SPI_INCORE__H
#define _SPI_INCORE__H

#include "spi_context.h"

#include <sys/sys_io.h>
#include <device.h>
#include <drivers/spi.h>

#define SPI_CFG(dev) ((struct spi_incore_cfg *) ((dev)->config_info))
#define SPI_DATA(dev) ((struct spi_incore_data *) ((dev)->driver_data))

#define SPI_REG(dev, offset) ((mem_addr_t) (SPI_CFG(dev)->base + (offset)))

/* Register Offsets */
#define REG_CFG1     0x00
#define REG_CFG2     0x04
#define REG_EN       0x08
#define REG_SR       0x0C
#define REG_TXR      0x10
#define REG_RXR      0x14
#define REG_RX_CRC   0x18
#define REG_TX_CRC   0x1C
#define REG_CFG_DLY  0x20
#define REG_PRESCLR  0x24
#define REG_CRC_POL  0x28
#define REG_CRCINOUT 0x2C

/* Offsets */
// defining SPI_CR1 register
#define IC_SPI_CPHA(x)             (x << 0 )
#define IC_SPI_CPOL(x)             (x << 1 )
#define IC_SPI_MSTR(x)             (x << 2 )
#define IC_SPI_RX_1ST              (1 << 6 )
#define IC_SPI_LSBFIRST(x)         (x << 7 )
#define IC_SPI_CLR_ST              (1 << 10)
#define IC_SPI_CRCL                (1 << 12)
#define IC_SPI_CRCEN               (1 << 13)
#define IC_SPI_DPLX(x)             (x << 14)
#define IC_SPI_DIS_SYNC(x)         (x << 15)
#define IC_SPI_TOTAL_BITS_TX(x)    (x << 16)
#define IC_SPI_TOTAL_BITS_RX(x)    (x << 24)

// defining SPI_CR2 register
#define IC_SPI_CSP(x)              (x << 2 )
#define IC_SPI_IDL_OUT(x)          (x << 3 )
#define IC_SPI_RX_TH_IE(x)         (x << 4 )
#define IC_SPI_ERR_IE(x)           (x << 5 )
#define IC_SPI_RXNE_IE(x)          (x << 6 )
#define IC_SPI_TXE_IE(x)           (x << 7 )
#define IC_SPI_RX_TH(x)            (x << 12)
#define IC_SPI_CRC_RIN             (1 << 16)
#define IC_SPI_CRC_ROUT            (1 << 17)
#define IC_SPI_PERIPH_SEL(x)       (x << 24)

#define IC_SPI_RXNE                (1 << 0)
#define IC_SPI_TXE                 (1 << 1)
#define IC_SPI_RX_FIFO_F           (1 << 2)
#define IC_SPI_TX_FIFO_NF          (1 << 3)
#define IC_SPI_OVRF                (1 << 10)
#define IC_SPI_BSY                 (1 << 11)
#define IC_SPI_FRLVL(x)            (x << 16)
#define IC_SPI_FTLVL(x)            (x << 13)

/* Values */
#define IC_MAX_PRESCALE 65536
#define IC_MIN_PRESCALE 2

/* Masks */
#define IC_SPI_TOTAL_BITS_TX_MASK  (0xFF << 16)
#define IC_SPI_TOTAL_BITS_RX_MASK  (0xFF << 24)
#define IC_SPI_DPLX_MASK           (0x1  << 14)
#define IC_SPI_RX_FIRST_MASK       (0x1  << 6)
#define IC_SPI_LSBFIRST_MASK       (0x1  << 7)

#define IC_TX_FIFO  (0x1F << 16)
#define IC_RX_FIFO  (0x1F << 24)

#define IC_SIMPLEX      0
#define IC_DUPLEX       1

/* Structure Declarations */

struct spi_incore_data {
	struct spi_context ctx;
};

struct spi_incore_cfg {
	uint32_t base;
	uint32_t f_sys;
    uint32_t max_slaves;
};

#endif /* _SPI_INCORE__H */
