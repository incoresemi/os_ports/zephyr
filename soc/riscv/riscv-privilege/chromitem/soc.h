#ifndef __RISCV_INCORE_CHROMITE_SOC_H__
#define __RISCV_INCORE_CHROMITE_SOC_H__
#include <soc_common.h>


#define RISCV_MTIME_BASE             0x0200BFF8
#define RISCV_MTIMECMP_BASE          0x02004000

#define RISCV_RAM_BASE               CONFIG_SRAM_BASE_ADDRESS
#define RISCV_RAM_SIZE               KB(CONFIG_SRAM_SIZE)

#endif
