set(OPENOCD_USE_LOAD_IMAGE NO)

board_runner_args(chromitem "--config=${BOARD_DIR}/support/ArtyXilinxTap.cfg")

include(${ZEPHYR_BASE}/boards/common/chromitem.board.cmake)

