# SPDX-License-Identifier: Apache-2.0
board_set_flasher_ifnset(chromitem)
board_set_debugger_ifnset(chromitem)
board_finalize_runner_args(chromitem
  --cmd-load "flash write_image erase"
  --cmd-verify "verify_image"
  )

