####################
Zephyr on Chormite-M
####################

This branch includes the Zephyr support for the Chromite-M SoC from InCore Semiconductors.

Steps to build on Ubuntu 18.04:
===============================


Update OS and Packages
----------------------

.. code-block:: bash

   $ sudo apt update
   $ sudo apt install --no-install-recommends git cmake ninja-build gperf \
        ccache dfu-util device-tree-compiler wget \
        python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
        make gcc gcc-multilib g++-multilib libsdl2-dev

Verify cmake version is greater than 3.13.1 by running::

  $ cmake --version
  >> cmake version 3.17.2

If not, then use the following steps to update cmake::

  $ wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add -
  $ sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
  $ sudo apt update
  $ sudo apt install cmake

Ubuntu 18.04 comes by default with python 3.6.9. Make sure you have atleast python 3.6.0 or above::

  $ python --version
  >> Python 3.6.9

If not, then use the following to upgrade python::

  $ sudo apt-get install python3.6
  $ pip3 install --upgrade pip

Zephyr also requires the RISC-V GNU Toolchain. Follow these steps to install RISC-V toolchain if it has not already been installed.

Clone the repository::

  $ git clone --recursive https://github.com/riscv/riscv-gnu-toolchain

On Ubuntu, the required dependencies can be installed by executing the following command::

  $ sudo apt-get install autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev


On Fedora/CentOS OS, the required dependencies can be installed by executing the following command::

  $ sudo yum install autoconf automake python3 libmpc-devel mpfr-devel gmp-devel gawk  bison flex texinfo patchutils gcc gcc-c++ zlib-devel expat-devel


To install the newlib cross-compiler, first pick an install path. If you choose, say, /tools/riscv-newlib, then run the following commands::

  $ export RISCV=/tools/riscv-newlib
  $ cd riscv-gnu-toolchain/
  $ ./configure --prefix=$RISCV --with-arch=rv64imac --with-abi=lp64 --with-cmodel=medany
  $ make

Add the RISC-V Newlib compiler bin folder to your $PATH variable by appending the following line in your .bashrc or .cshrc::

  $ export PATH=$PATH:/tools/riscv-newlib/bin

You should now be able to use riscv64-unknown-elf-gcc and it's cousins.

Get Zephyr and Meta packages
----------------------------

Now install the west package::

  $ pip3 install --user -U west

You can ignore ``--user`` if the above throws an error.

Create a folder for the project (say zephyrproject)::

  $ mkdir zephyrproject
  $ cd zephyrproject
  $ git clone https://gitlab.com/incoresemi/os_ports/zephyr.git
  $ cd zephyr
  $ git checkout chromiteM
  $ pip3 install -r scripts/requirements.txt
  $ west init -l .

Now, set the environment variable "CROSS_COMPILE" to point to your RISC-V toolchain installation::

  $ export CROSS_COMPILE=/tools/riscv-newlib/bin/riscv64-unknown-elf-

Also, set the following environment variable::

  $ export ZEPHYR_TOOLCHAIN_VARIANT=cross-compile


Build Samples
=============

We can now run sample programs on the Chromite M SoC

Hello-World
-----------

Create the zephyr.elf::

  $ cd zephyrproject/zephyr
  $ west build -p -b chromitem samples/hello_world

You can load this elf on to the chromiteM SoC through a debugger using the ``west debug`` command as
shown below.

.. code-block:: shell-session
  
  $ west debug

  -- west debug: rebuilding
  ninja: no work to do.
  -- west debug: using runner chromitem
  Open On-Chip Debugger 0.10.0+dev-00858-g4f9e2d717-dirty (2020-05-25-18:58)
  Licensed under GNU GPL v2
  For bug reports, read
  	http://openocd.org/doc/doxygen/bugs.html
  Info : auto-selecting first available session transport "jtag". To override use 'transport select <transport>'.
  Info : Simple Register based Bscan Tunnel Selected
  GNU gdb (GDB) 9.1
  Copyright (C) 2020 Free Software Foundation, Inc.
  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
  This is free software: you are free to change and redistribute it.
  There is NO WARRANTY, to the extent permitted by law.
  Type "show copying" and "show warranty" for details.
  This GDB was configured as "--host=x86_64-pc-linux-gnu --target=riscv64-unknown-elf".
  Type "show configuration" for configuration details.
  For bug reporting instructions, please see:
  <http://www.gnu.org/software/gdb/bugs/>.
  Find the GDB manual and other documentation resources online at:
      <http://www.gnu.org/software/gdb/documentation/>.
  
  For help, type "help".
  Type "apropos word" to search for commands related to "word"...
  Reading symbols from /scratch/git-repo/incoresemi/os_ports/zephyrproject/zephyr/build/zephyr/zephyr.elf...
  Info : ftdi: if you experience problems at higher adapter clocks, try the command "ftdi_tdo_sample_edge falling"
  Info : clock speed 14000 kHz
  Info : JTAG tap: riscv.cpu tap/device found: 0x13631093 (mfg: 0x049 (Xilinx), part: 0x3631, ver: 0x1)
  Info : datacount=12 progbufsize=0
  Warn : We won't be able to execute fence instructions on this target. Memory may not always appear consistent. (progbufsize=0, impebreak=0)
  Info : Examined RISC-V core; found 1 harts
  Info :  hart 0: XLEN=64, misa=0x8000000000101105
  Info : Listening on port 3333 for gdb connections
      TargetName         Type       Endian TapName            State       
  --  ------------------ ---------- ------ ------------------ ------------
   0* riscv.cpu.0        riscv      little riscv.cpu          halted
  
  Info : Listening on port 6333 for tcl connections
  Info : Listening on port 4444 for telnet connections
  Remote debugging using :3333
  Info : accepting 'gdb' connection on tcp/3333
  0x000000000001014c in ?? ()
  Loading section vector, size 0x10 lma 0x80000000
  Loading section exceptions, size 0x1ee lma 0x80000010
  Loading section text, size 0x19c0 lma 0x80000200
  Loading section initlevel, size 0x60 lma 0x80001bc0
  Loading section sw_isr_table, size 0x1e0 lma 0x80001c20
  Loading section rodata, size 0x358 lma 0x80001e00
  Loading section datas, size 0x24 lma 0x80002158
  Loading section devices, size 0x60 lma 0x80002180
  Loading section _k_mutex_area, size 0x20 lma 0x800021e0
  Start address 0x0000000080000000, load size 8698
  Transfer rate: 202 KB/sec, 966 bytes/write.
  Section vector, range 0x80000000 -- 0x80000010: matched.
  Section exceptions, range 0x80000010 -- 0x800001fe: matched.
  Section text, range 0x80000200 -- 0x80001bc0: matched.
  Section initlevel, range 0x80001bc0 -- 0x80001c20: matched.
  Section sw_isr_table, range 0x80001c20 -- 0x80001e00: matched.
  Section rodata, range 0x80001e00 -- 0x80002158: matched.
  Section datas, range 0x80002158 -- 0x8000217c: matched.
  Section devices, range 0x80002180 -- 0x800021e0: matched.
  Section _k_mutex_area, range 0x800021e0 -- 0x80002200: matched.
  (gdb) 

In order to see the printf statements of your application you will need to connect to the serial
port (on a different terminal) using the following command

.. code-block:: shell-session

   $ sudo miniterm -f direct --eol CRLF /dev/ttyUSB1 115200

   --- Miniterm on /dev/ttyUSB1  115200,8,N,1 ---
   --- Quit: Ctrl+] | Menu: Ctrl+T | Help: Ctrl+T followed by Ctrl+H ---
   
     _____        _____
    |_   _|      / ____|
      | |  _ __ | |     ___  _ __ ___
      | | | '_ \| |    / _ \| '__/ _ \
     _| |_| | | | |___| (_) | | |  __/
    |_____|_| |_|\_____\___/|_|  \___|
   
   
               Chromite M SoC
               Version: 1.0.0
     Build Date: 2020-05-30 21:07:54 IST
   Copyright (c) 2020 InCore Semiconductors
     Available under Apache v2.0 License

Once the above gdb terminal is accessible you can perform regular gdb commands. 
Using the "continue" or "c" command you should see the following on the serial console:

.. code-block:: shell-session

  *** Booting Zephyr OS build v2.3.0-rc1-153-g4b55125ba53e  ***
  Hello World! chromitem
