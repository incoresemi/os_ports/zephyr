/*
 * Copyright (c) 2020 Incore Semiconductors Pvt. Ltd.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file Header file for the Incore PWM driver.
 */

#ifndef ZEPHYR_DRIVERS_PWM_INCORE_DRIVER_H_
#define ZEPHYR_DRIVERS_PWM_INCORE_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <device.h>
#include <zephyr/types.h>

/* Macros */
#define PWM_ENABLE                      0
#define PWM_START                       1
#define PWM_OUTPUT_ENABLE               2
#define PWM_OUTPUT_POLARITY             3
#define RESET_COUNTER                   4
#define PWM_OUTPUT_COMPLEMNT            5
#define HALF_PERIOD_INTERRUPT_ENABLE    6
#define FALL_INTERRUPT_ENABLE           7
#define RISE_INTERRUPT_ENABLE           8
#define READ_ONLY_HALF_PERIOD_INTERRUPT 0x00000200
#define READ_ONLY_FALL_INTERRUPT        0x00000400
#define READ_ONLY_RISE_INTERRUPT        0x00000800
#define PWM_LOAD_VALUES                 12

#define CLOCK_SELECTOR                  1
#define CLOCK_PRESCALAR                 50
#define CLOCK_PRESCALAR_MAX             (1 << 15) -1
#define PWM_DEADBAND_VALUE				0x02

#define PWM_REG(z_config, _offset) ((mem_addr_t) ((z_config)->base + _offset))

/* Register Offsets */
#define REG_CLOCK                  0x00
#define REG_CONTROL(_channel)      (_channel << 4) + 0x04
#define REG_PERIOD(_channel)       (_channel << 4) + 0x08
#define REG_DUTY(_channel)         (_channel << 4) + 0x0C
#define REG_DEAD_BAND(_channel)    (_channel << 4) + 0x10

__syscall int incore_pwm_set_deadband_delay(struct device *dev, uint32_t pwm, uint16_t deadband);
__syscall int incore_pwm_set_clock_register(struct device *dev, uint16_t prescalar, bool source);

#ifdef __cplusplus
}
#endif

#include <syscalls/pwm_incore.h>

#endif /* ZEPHYR_DRIVERS_PWM_INCORE_DRIVER_H_ */
